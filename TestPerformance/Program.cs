﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Infrastructure.DependencyResolution;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using BenchmarkDotNet;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;
using System.Linq.Dynamic;
namespace TestPerformance
{
    public class Program
    {
        static Program()
        {
            //LoadPersonNoTracking();
        }

        static void Main(string[] args)
        {
            //using (var context = new EntityContext())
            //{

            //    for (int i = 0; i < 10; i++)
            //    {
            //        var zoo = new Zoo(context) { Name = $"Zoo{i}" };
            //        var cat = new Cat(context) { Name = $"Cat{i}", DateBirth = DateTime.Now };
            //        cat.Zoo = zoo;
            //        context.SaveChanges();
            //    }
            //    var aa = context.ChangeTracker.Entries();
            //    context.SaveChanges();
            //    context.Dispose();
            //}

            using (var context = new EntityContext())
            {
                var cats = context.GetEntities(typeof(Cat));
                var aa = context.ChangeTracker.Entries();
                var zoo = "Zoo";
                //foreach (var item in ((IQueryable<dynamic>)cats.Select("Zoo")).Distinct())
                //{


                //}
                foreach (var item in ((IQueryable<dynamic>)cats.Select("Zoo")).whe)
                {


                }
                aa = context.ChangeTracker.Entries();
            }
        }

        public static IQueryable<TResult> Select<TResult>(IQueryable source, Type entityType, string[] columns)
        {
            var sourceType = source.ElementType;
            var resultType = typeof(TResult);
            var parameter = Expression.Parameter(sourceType, "e");
            var bindings = columns.Select(column => Expression.Bind(
                resultType.GetProperty(column), Expression.PropertyOrField(parameter, column)));
            var body = Expression.MemberInit(Expression.New(resultType), bindings);
            var selector = Expression.Lambda(body, parameter);
            return source.Provider.CreateQuery<TResult>(
                Expression.Call(typeof(Queryable), "Select", new Type[] { sourceType, resultType },
                    source.Expression, Expression.Quote(selector)));
        }

        private ObservableCollection<int> _observableCollection1;

        public ObservableCollection<int> ObservableCollection1
        {
            get { return _observableCollection1; }
            set { _observableCollection1 = value; }
        }


        private static void Coll_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
        }

        [Benchmark]
        public static void LoadPersonNoTracking()
        {
            using (var context = new EntityContext())
            {
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();
                //var persons = context.Persons;
                //foreach (var person in persons)
                //{
                //    var fullName = person.FullName;
                //}
                //var countOfPersons = persons.Count();
                stopwatch.Stop();
                var interval = stopwatch.ElapsedMilliseconds;
                System.Diagnostics.Debug.WriteLine(interval);

            }
        }

        private static void CreatePersons()
        {
            using (var context = new EntityContext())
            {
                //context.Configuration.AutoDetectChangesEnabled = false;
                for (int i = 0; i < 242; i++)
                {
                    new Person(context) { FullName = $"Full name {i}" };
                }
                //context.Configuration.AutoDetectChangesEnabled = true;
                context.SaveChanges();
            }
        }
    }
}
