``` ini

BenchmarkDotNet=v0.10.12, OS=Windows 10 Redstone 3 [1709, Fall Creators Update] (10.0.16299.192)
Intel Core i7-4770 CPU 3.40GHz (Haswell), 1 CPU, 8 logical cores and 4 physical cores
Frequency=3312642 Hz, Resolution=301.8739 ns, Timer=TSC
  [Host]     : .NET Framework 4.7 (CLR 4.0.30319.42000), 64bit RyuJIT-v4.7.2600.0
  DefaultJob : .NET Framework 4.7 (CLR 4.0.30319.42000), 64bit RyuJIT-v4.7.2600.0


```
|               Method |     Mean |     Error |    StdDev |
|--------------------- |---------:|----------:|----------:|
| LoadPersonNoTracking | 1.609 ms | 0.0062 ms | 0.0048 ms |
