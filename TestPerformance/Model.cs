﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestPerformance
{
    public abstract class EntityBase : INotifyPropertyChanged
    {

        protected EntityBase()
        {

        }

        protected EntityBase(EntityContext context)
        {
            Context = context;
            AddToContext();
        }
        private Guid guid;

        [Key]
        public Guid Guid
        {
            get
            {
                if (guid == Guid.Empty)
                {
                    guid = Guid.NewGuid();
                }
                return guid;
            }
            set { guid = value; }
        }


        public EntityContext Context { get; set; }

        void AddToContext()
        {
            Context.Entry(this).State = System.Data.Entity.EntityState.Added;
        }

        #region Notify

        /// <summary>
        /// Property Changed event
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Fire the PropertyChanged event
        /// </summary>
        /// <param name="propertyName">Name of the property that changed (defaults from CallerMemberName)</param>
        protected void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }

    #region Test performance

    public class Person : EntityBase
    {
        public Person() { }

        public Person(EntityContext context) : base(context) { }
        public string FullName { get; set; }
    }

    public class Passport : EntityBase
    {
        protected Passport()
        {

        }
        public Passport(EntityContext context) : base(context)
        {

        }
        public string Number { get; set; }

        public virtual Person Person { get; set; }
    }
    #endregion

    #region Test interface

    [Table(nameof(Animal))]
    public class Animal : EntityBase
    {
        public Animal()
        {

        }

        public Animal(EntityContext context) : base(context)
        {

        }
        public Guid? ZooGuid { get; set; }
        public virtual BaseZoo Zoo { get; set; }
    }

    [Table(nameof(BaseZoo))]
    public class BaseZoo : EntityBase
    {
        public BaseZoo()
        {

        }

        public BaseZoo(EntityContext context) : base(context)
        {

        }

        public string Name { get; set; }
        public virtual ICollection<Animal> Animals { get; set; }
    }

    [Table(nameof(Zoo))]
    public class Zoo : BaseZoo
    {
        public Zoo()
        {

        }
        public Zoo(EntityContext context) : base(context)
        {

        }
    }

    [Table(nameof(Cat))]
    public class Cat : Animal
    {
        public Cat()
        {

        }

        public Cat(EntityContext context) : base(context)
        {

        }

        public int Age { get; set; }
        public string Color { get; set; }
        public DateTime DateBirth { get; set; }
        public string Name { get; set; }
    }

    public interface IZoo : IAnimal
    {
        Zoo Zoo { get; set; }
    }

    public interface IAnimal
    {
        int Age { get; set; }

        string Color { get; set; }

        DateTime DateBirth { get; set; }

        string Name { get; set; }
    }

    #endregion

}
