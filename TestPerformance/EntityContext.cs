﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestPerformance
{
    public class EntityContext : DbContext
    {
        //public DbSet<Person> Persons { get; set; }
        public DbSet<Zoo> Zoos { get; set; }
        public DbSet<Animal> Animals { get; set; }
        public DbSet<Cat> Cats { get; set; }

        public EntityContext() : base("data source=localhost;initial catalog=TestPerformance;integrated security=True;")
        {
            this.Database.CreateIfNotExists();
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;
            var baseContext = ((IObjectContextAdapter)this).ObjectContext;
            baseContext.ObjectMaterialized += BaseContext_ObjectMaterialized;
            //this.Configuration.LazyLoadingEnabled = false;
        }



        private void BaseContext_ObjectMaterialized(object sender, System.Data.Entity.Core.Objects.ObjectMaterializedEventArgs e)
        {
            var entityBase = e.Entity as EntityBase;
            if (entityBase != null && entityBase.Context == null)
            {
                entityBase.Context = this;
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Ignore<EntityBase>();
            modelBuilder.Entity<EntityBase>().Ignore(e => e.Context);
            modelBuilder.Entity<EntityBase>().HasKey(t => t.Guid);
            modelBuilder.Entity<BaseZoo>().HasMany(z => z.Animals).WithOptional(a => a.Zoo).HasForeignKey(z => z.ZooGuid).WillCascadeOnDelete();
            modelBuilder.Entity<Animal>().HasOptional(a => a.Zoo).WithMany(z => z.Animals).HasForeignKey(z => z.ZooGuid).WillCascadeOnDelete();
            modelBuilder.Entity<Zoo>().ToTable(nameof(Zoo));
            modelBuilder.Entity<Cat>().ToTable(nameof(Cat));
            //modelBuilder.Ignore<IZoo>();
            //modelBuilder.Entity<IZoo>().Ignore(z => z.Zoo);
        }


        public IQueryable<T> GetEntities<T>() where T : EntityBase
        {
            try
            {
                var type = typeof(T);
                return GetEntities(type).Cast<T>();
            }
            catch (Exception ex)
            {
                throw new ArgumentException("Invalid Entity Type supplied for Lookup", ex);
            }
        }

        public IQueryable<T> GetEntities<T>(IEnumerable<string> properties) where T : EntityBase
        {
            var result = GetEntities(typeof(T)).Cast<T>();
            ;
            return properties.Aggregate(result, (current, property) => current.Include(property)).Cast<T>();
        }

        public IQueryable GetEntities(Type typeOfEntity)
        {
            try
            {
                return Set(typeOfEntity).AsQueryable();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}

